package netcdf;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;
import com.tcb.netcdf.AmberPrmtop;

public class AmberPrmtopTest {

	private ClassLoader classLoader = getClass().getClassLoader();
	private Path inFile;
	private AmberPrmtop prmtop;
	
	@Before
	public void setUp() throws Exception {
		this.inFile = Paths.get(classLoader.getResource("DnaK-ATP_solv_noWater.prmtop").toURI());
		this.prmtop = AmberPrmtop.read(inFile);
	}
	
	@Test
	public void testGetAtoms() {
		List<Atom> atoms = prmtop.getAtoms();
		assertEquals(9257,atoms.size());
		Set<Residue> residues = atoms.stream()
				.map(a -> a.getResidue())
				.collect(Collectors.toSet());
		assertEquals(630, residues.size());
		int[] resindexes = atoms.stream()
				.map(a -> a.getResidue().getIndex())
				.mapToInt(i -> i)
				.toArray();
		
		assertEquals(1,resindexes[0]);
		assertEquals(1,resindexes[1]);
		assertEquals(1,resindexes[2]);
		assertEquals(1,resindexes[8]);
		assertEquals(2,resindexes[9]);
		assertEquals(2,resindexes[30]);
		assertEquals(3,resindexes[31]);
		assertEquals(629,resindexes[9255]);
		assertEquals(630,resindexes[9256]);
		
		String[] resnames = atoms.stream()
				.map(a -> a.getResidue().getName())
				.toArray(String[]::new);
		
		assertEquals("GLY",resnames[0]);
		assertEquals("LYS",resnames[11]);
		assertEquals("Na+",resnames[9256]);
		
		String[] atomnames = atoms.stream()
				.map(a -> a.getName())
				.toArray(String[]::new);
		
		assertEquals("N",atomnames[0]);
		assertEquals("H",atomnames[10]);
		assertEquals("Na+",atomnames[9256]);

	}

}
