package netcdf;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.netcdf.FortranFormat;
import com.tcb.netcdf.FortranFormatFactory;

public class FortranFormatFactoryTest {

	private FortranFormatFactory fac;

	@Before
	public void setUp() {
		this.fac = new FortranFormatFactory();
	}
	
	@Test
	public void testParseFormatLineInteger() {
		String s = "%FORMAT(10I8)";
		FortranFormat f = fac.parseFormatLine(s);
		
		assertEquals(Integer.class,f.getType());
		assertEquals(10,f.getMaxElementsPerLine());
		assertEquals(8,f.getWordSize());
	}
	
	@Test
	public void testParseFormatLineString() {
		String s = "%FORMAT(20a4)";
		FortranFormat f = fac.parseFormatLine(s);
		
		assertEquals(String.class,f.getType());
		assertEquals(20,f.getMaxElementsPerLine());
		assertEquals(4,f.getWordSize());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParseFormatLineInvalidStart() {
		String s = "%FORMAT(20a4) abc";
		FortranFormat f = fac.parseFormatLine(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParseFormatLineInvalidEnd() {
		String s = "abc%FORMAT(20a4)";
		FortranFormat f = fac.parseFormatLine(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParseFormatLineDouble() {
		String s = "abc%FORMAT(5E16.8)";
		FortranFormat f = fac.parseFormatLine(s);
	}

}
