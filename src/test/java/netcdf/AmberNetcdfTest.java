package netcdf;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tcb.netcdf.AmberNetcdfFile;

public class AmberNetcdfTest {

	private ClassLoader classLoader = getClass().getClassLoader();
	private Path inFile;
	private AmberNetcdfFile nc;
	
	@Before
	public void setUp() throws Exception {
		this.inFile = Paths.get(classLoader.getResource("trpzip2.gb.nc").toURI());
		this.nc = AmberNetcdfFile.open(inFile);
	}
	
	@After
	public void tearDown() throws Exception {
		nc.close();
	}
	
	@Test
	public void testGetFrameCount() {
		assertEquals(1201,nc.getFrameCount());
	}
	
	@Test
	public void testGetAtomCount() {
		assertEquals(220,nc.getAtomCount());
	}

	@Test
	public void testGetCoordinates() throws Exception {
		assertEquals(9.3637d,nc.getCoordinates(1)[1][1],0.0001);
		assertEquals(-4.0991d,nc.getCoordinates(0)[0][0],0.0001);
		assertEquals(-3.27899d,nc.getCoordinates(142)[7][2],0.0001);
		assertEquals(-0.25257d,nc.getCoordinates(0)[0][2],0.0001);
		assertEquals(0.90602d,nc.getCoordinates(0)[7][2],0.0001);
	}

}
