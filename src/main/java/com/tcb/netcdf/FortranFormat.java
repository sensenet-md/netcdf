package com.tcb.netcdf;

import java.util.List;

public interface FortranFormat<T> {
	
	public int getMaxElementsPerLine();
	public int getWordSize();
	public Class<T> getType();
	public List<T> parse(List<String> lines);
	
}
