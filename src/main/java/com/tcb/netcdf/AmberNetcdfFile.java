package com.tcb.netcdf;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

import ucar.ma2.ArrayDouble;
import ucar.ma2.ArrayFloat;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class AmberNetcdfFile {

	private NetcdfFile nc;
	private Variable coors;
	private int[] shape;
	
	private AmberNetcdfFile(NetcdfFile nc) {
		this.nc = nc;
		this.coors = nc.findVariable("coordinates");
		this.shape = new int[] {1,getAtomCount(),3};
	}
	
	@Override
	protected void finalize() throws Exception {
		close();
	}
	
	public void close() throws IOException {
		if(nc!=null) nc.close();
	}
		
	public static AmberNetcdfFile open(Path path) throws IOException {
		NetcdfFile nc = null;
		nc = NetcdfFile.open(path.toString(), null);
		nc.setImmutable();
		return new AmberNetcdfFile(nc);
	}
	
	public int getFrameCount() {
		return coors.getShape(0);
	}
	
	public int getAtomCount() {
		return coors.getShape(1);
	}
		
	public synchronized double[][] getCoordinates(int frame) throws Exception {
		ArrayFloat.D3 data = null;
		final int atomCount = getAtomCount();
		double[][] result = new double[atomCount][3];
		int[] origin = new int[] {frame,0,0};
		
		data = (ArrayFloat.D3) coors.read(origin, shape);
		
		for(int i=0;i<atomCount;i++) {
			for(int j=0;j<3;j++) {
				result[i][j] = data.get(0, i, j);
			}
		}
		
		return result;
	}
	
}
