package com.tcb.netcdf;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFortranFormat<T> implements FortranFormat<T> {

	protected int wordSize;
	protected int maxElementsPerLine;
	
	protected abstract T parse(String s);
	
	protected AbstractFortranFormat(int wordSize, int maxElementsPerLine) {
		this.wordSize = wordSize;
		this.maxElementsPerLine = maxElementsPerLine;
	}
		
	public int getMaxElementsPerLine() {
		return maxElementsPerLine;
	}
	
	public int getWordSize() {
		return wordSize;
	}
	
	public List<T> parse(List<String> lines) {
		List<T> result = new ArrayList<>();
		for(String line:lines) {
			while(!line.isEmpty()) {
				String field = line.substring(0, wordSize).trim();
				line = line.substring(wordSize);
				result.add(parse(field));
			}
		}
		return result;
	}
}
