package com.tcb.netcdf;

import java.util.List;

public class StringFortranFormat extends AbstractFortranFormat<String> {
	
	public StringFortranFormat(int wordSize, int maxElementsPerLine) {
		super(wordSize,maxElementsPerLine);
	}
	
	@Override
	public Class<String> getType() {
		return String.class;
	}

	@Override
	protected String parse(String s) {
		return s.trim();
	}

}
