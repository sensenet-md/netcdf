package com.tcb.netcdf;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Enums;
import com.tcb.atoms.atoms.Atom;
import com.tcb.common.util.SafeMap;

public class AmberPrmtop {
	
	private List<Atom> atoms;
	
	private enum RecordType {
		ATOM_NAME, RESIDUE_POINTER, RESIDUE_LABEL,
		POINTERS;
	}
	
	private AmberPrmtop(List<Atom> atoms) {
		this.atoms = atoms;
	}
	
	public List<Atom> getAtoms(){
		return atoms;
	}
							
	public static AmberPrmtop read(Path path) throws IOException {
		List<String> atomNames = new ArrayList<>();
		List<Integer> resIndexes = new ArrayList<>();
		List<String> resNames = new ArrayList<>();
		List<Integer> pointers = new ArrayList<>();
		Map<RecordType,List<String>> records = getRecords(path);
								
		List<RecordType> types = Arrays.asList(
				RecordType.POINTERS, RecordType.RESIDUE_POINTER,
				RecordType.RESIDUE_LABEL, RecordType.ATOM_NAME);

		for(RecordType type:types) {
			List<String> lines = records.get(type);
			FortranFormat<?> f = new FortranFormatFactory().parseFormatLine(lines.get(1));
			lines = lines.subList(2, lines.size());
			List<?> content = f.parse(lines);
			switch(type) {
			case ATOM_NAME: atomNames = (List<String>) content; break;
			case POINTERS: pointers = (List<Integer>) content; break;
			case RESIDUE_LABEL: resNames = parseResnamePointers((List<String>) content, resIndexes); break;
			case RESIDUE_POINTER: resIndexes = parseResindexPointers((List<Integer>) content, pointers.get(0)); break;
			default: throw new IllegalArgumentException();
			}
		}
	
		int atomCount = pointers.get(0);
		List<Atom> atoms = new ArrayList<>();
		for(int i=0;i<atomCount;i++) {
			Atom a = Atom.create(
					atomNames.get(i),
					resIndexes.get(i),
					resNames.get(i), "", "","");
			atoms.add(a);
		}
		return new AmberPrmtop(atoms);
	}
	
	private static Map<RecordType,List<String>> getRecords(Path path) throws IOException {
		Map<RecordType,List<String>> records = new SafeMap<>();
		List<String> lines = new ArrayList<>();
		RecordType type = null;
		try(BufferedReader rdr = new BufferedReader(new FileReader(path.toFile()))) {
			String line = rdr.readLine();
			while(line!=null) {
				if(line.startsWith("%FLAG")) {
					if(type!=null) records.put(type, lines);
					lines = new ArrayList<>();
					String flag = line.split("\\s+")[1];
					type = Enums.getIfPresent(RecordType.class, flag).orNull();
				}
				lines.add(line);
				line = rdr.readLine();
			} 	
		}
		return records;
	}
			
	private static List<Integer> parseResindexPointers(List<Integer> resindexPointers, int atomCount){
		Iterator<Integer> it = resindexPointers.iterator();
		List<Integer> result = new ArrayList<>();
		Integer curResindex = 0;
		Integer curPointer = it.next();
		for(int i=0;i<atomCount;i++) {
			Integer atomNumber = i + 1;
			if(curPointer.equals(atomNumber)) {
				if(it.hasNext()) curPointer = it.next();
				curResindex+=1;
			}
			result.add(curResindex);
		}
		return result;
	}
	
	private static List<String> parseResnamePointers(List<String> resnamePointers, List<Integer> resIndexes){
		Iterator<String> it = resnamePointers.iterator();
		List<String> resnames = new ArrayList<>();
		Integer lastResindex = null;
		String resname = null;
		for(Integer resIndex:resIndexes) {
			if(!resIndex.equals(lastResindex)) {
				lastResindex = resIndex;
				if(it.hasNext()) resname = it.next();
			}
			resnames.add(resname);
		}
		return resnames;
	}
	
}
