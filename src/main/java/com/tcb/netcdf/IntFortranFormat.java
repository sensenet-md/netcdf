package com.tcb.netcdf;

import java.util.ArrayList;
import java.util.List;

public class IntFortranFormat extends AbstractFortranFormat<Integer> {
	
	public IntFortranFormat(int wordSize, int maxElementsPerLine) {
		super(wordSize,maxElementsPerLine);
	}
	
	@Override
	public Class<Integer> getType() {
		return Integer.class;
	}

	@Override
	protected Integer parse(String s) {
		return Integer.parseInt(s);
	}

	

}
