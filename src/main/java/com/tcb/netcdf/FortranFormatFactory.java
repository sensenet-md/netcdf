package com.tcb.netcdf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FortranFormatFactory {

	private FortranFormat parse(String s) {
		String[] t = tokenize(s);
		int size = t.length;
		
		if(size==3) {
			if(t[1].equals("I"))
				return new IntFortranFormat(
						Integer.parseInt(t[2]),
						Integer.parseInt(t[0])
						);
			else if(t[1].equals("a"))
				return new StringFortranFormat(
						Integer.parseInt(t[2]),
						Integer.parseInt(t[0]));
		}
		
		throw new IllegalArgumentException("Unsupported format");
	}
	
	private String[] tokenize(String s) {
		List<String> tokens = new ArrayList<>();
		StringBuilder buf = new StringBuilder();
		char[] cs = s.toCharArray();
		boolean lastWasDigit = isDigit(cs[0]);
		
		for(char c:cs) {
			boolean isDigit = isDigit(c);
			if(lastWasDigit == isDigit) {
				buf.append(c);
				continue;
			}
			tokens.add(buf.toString());
			buf = new StringBuilder();
			buf.append(c);
			lastWasDigit = isDigit;
		}
		tokens.add(buf.toString());		
		return tokens.toArray(new String[0]);
	}
	
	private boolean isDigit(char c) {
		return Character.isDigit(c);
	}
	
	public FortranFormat parseFormatLine(String s) {
		s = s.trim();
		if(!s.startsWith("%FORMAT(") || !s.endsWith(")"))
			throw new IllegalArgumentException("Not a format line");
		s = s.replaceFirst("%FORMAT\\((.*)\\)", "$1");
		return parse(s);
	}
	
}
